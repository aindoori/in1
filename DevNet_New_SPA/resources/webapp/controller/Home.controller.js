sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function (Controller) {
	"use strict";

	return Controller.extend("devnet-neo.DevNet_New_SPA.controller.Home", {
		onInit: function () {
			//alert("Initialize Application");
			var listOfLinks = this.getView().byId("listOfLinks");
			listOfLinks.setVisible(false);
			//listOfLinks.setText("yo what up fam <br> homies this is a test");
			//seems like line breaks aren't possible here
		},

		toggleListOfLinks: function () {
			var listOfLinks = this.getView().byId("listOfLinks");
			var toggleListButton = this.getView().byId("showHideMenu");

			//if the toggle button is pressed, we show the thing. if it is unpressed, we hide it
			listOfLinks.setVisible(toggleListButton.getPressed());
		}

	});
});